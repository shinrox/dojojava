package br.com.bluesoft.controleEstoque;

import br.com.bluesoft.controleEstoque.model.Pessoa;
import br.com.bluesoft.controleEstoque.model.Produto;
import br.com.bluesoft.controleEstoque.services.LojaService;
import br.com.bluesoft.controleEstoque.services.LojaServiceImpl;

import java.util.Random;

public class Main {
    private static LojaService lojaService = new LojaServiceImpl();

    public static void main(String[] args) {
        Pessoa pessoa = new Pessoa("Ricardo");
        Produto produto = new Produto("Coca-cola", 4.90);

        lojaService.comprar(pessoa, produto, 100);

        for (int i = 0; i <= 5; i++) {
            lojaService.vender(pessoa, produto, randomInt(40, 10));
        }
    }

    public static Integer randomInt(Integer max, Integer min) {
        Random rand = new Random();
        return rand.nextInt((max - 1) + min) + 1;
    }

}
