package br.com.bluesoft.controleEstoque.model;

public class Estoque {

    private Integer quantidade;

    public Estoque() {
        this.setQuantidade(0);
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
