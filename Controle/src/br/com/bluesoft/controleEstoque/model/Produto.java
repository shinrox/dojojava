package br.com.bluesoft.controleEstoque.model;

public class Produto {

    private String descricao;

    private Double preco;

    private Estoque estoque;

    public Produto() {
        this.setEstoque(new Estoque());
    }

    public Produto(String descricao, double preco) {
        this.setDescricao(descricao);
        this.setPreco(preco);
        this.setEstoque(new Estoque());
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    public Estoque getEstoque() {
        return estoque;
    }

    public void setEstoque(Estoque estoque) {
        this.estoque = estoque;
    }

    public Integer getQuantidade() {
        return this.estoque.getQuantidade();
    }
}
