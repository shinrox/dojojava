package br.com.bluesoft.controleEstoque.services;

import br.com.bluesoft.controleEstoque.model.Pessoa;
import br.com.bluesoft.controleEstoque.model.Produto;

public interface LojaService {

    void vender(Pessoa pessoa, Produto produto, Integer quantidade);
    void comprar(Pessoa pessoa, Produto produto, Integer quantidade);
}
