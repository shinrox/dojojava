package br.com.bluesoft.controleEstoque.services;


import br.com.bluesoft.controleEstoque.model.Estoque;
import br.com.bluesoft.controleEstoque.model.Pessoa;
import br.com.bluesoft.controleEstoque.model.Produto;

public class LojaServiceImpl implements LojaService {

    public void vender(Pessoa pessoa, Produto produto, Integer quantidade) {
        Estoque estoque = produto.getEstoque();

        printDivider();

        if (podeVender(estoque, quantidade)) {
            estoque.setQuantidade(estoque.getQuantidade() - quantidade);

            System.out.println(pessoa.getNome() + " efetuou a venda de " + quantidade + " unidades de " + produto.getDescricao());

            if (estoque.getQuantidade() <= 10) {
                System.out.println("Estoque de " + produto.getDescricao() + " está baixo, você deve comprar mais");
            }

            System.out.println("Estoque atual: " + estoque.getQuantidade());
        } else {
            System.out.println(pessoa.getNome() + " tentou vender " + quantidade + " unidades de " + produto.getDescricao());
            System.out.println("Estoque de " + produto.getDescricao() + " insuficiente para venda");
            printEstoqueAtual(estoque);
        }

    }

    public void comprar(Pessoa pessoa, Produto produto, Integer quantidade) {
        Estoque estoque = produto.getEstoque();
        estoque.setQuantidade(estoque.getQuantidade() + quantidade);
        printDivider();
        System.out.println(pessoa.getNome() + " realizou a compra de " + quantidade + " unidades de " + produto.getDescricao());
        printEstoqueAtual(estoque);
    }

    private void printEstoqueAtual(Estoque estoque) {
        System.out.println("Estoque atual: " + estoque.getQuantidade());
    }

    private void printDivider() {
        System.out.println("========================");
    }

    private boolean podeVender(Estoque estoque, Integer quantidade) {
        return estoque.getQuantidade() - quantidade >= 0;
    }
}
